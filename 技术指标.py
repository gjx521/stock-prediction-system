import tkinter as tk
import pandas as pd
import ta
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker


# 定义文件列表
# files = ['600519.csv', 'AAPL.csv', '腾讯.csv']
def check(stock_codes, result_text):
    files = [f'data/{code}.csv' for code in stock_codes]
    plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
    plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
    # 遍历文件列表
    for file in files:
        # 读取数据文件
        df = pd.read_csv(file, encoding='utf-8')

        # 处理列名和日期格式
        df.columns = ['股票名称', '股票代码', '日期', '开盘', '收盘', '最高', '最低', '成交量', '成交额', '振幅',
                      '涨跌幅',
                      '涨跌额', '换手率']

        # 填充缺失值
        df.fillna(method='ffill', inplace=True)

        # 计算移动平均线（MA）
        df['MA10'] = ta.trend.sma_indicator(df['收盘'], window=10)
        df['MA20'] = ta.trend.sma_indicator(df['收盘'], window=20)

        # 计算相对强弱指标（RSI）
        # 当RSI值超过70时，表示资产可能处于超买状态，可能发生价格回调；
        # 当RSI值低于30时，表示资产可能处于超卖状态，可能发生价格反弹。
        df['RSI'] = ta.momentum.rsi(df['收盘'], window=14)

        # 计算布林带（Bollinger Bands）
        # 当价格触及或穿越上轨线时，可能暗示资产处于超买状态，可能发生价格回调；
        # 当价格触及或穿越下轨线时，可能暗示资产处于超卖状态，可能发生价格反弹。
        df['BB_upper'], df['BB_middle'], df['BB_lower'] = ta.volatility.bollinger_hband_indicator(df['收盘'],
                                                                                                  window=20), ta.volatility.bollinger_mavg(
            df['收盘'], window=20), ta.volatility.bollinger_lband_indicator(df['收盘'], window=20)

        # 显示计算后的数据
        fig, ax = plt.subplots(1, 1, figsize=(12, 6))
        plt.plot(df['日期'], df['收盘'], label='收盘价')
        plt.plot(df['日期'], df['MA10'], label='MA10')
        plt.plot(df['日期'], df['MA20'], label='MA20')
        length = len(df['日期'])
        ax.xaxis.set_major_locator(ticker.MultipleLocator(base=length / 5))  # 解决X轴密集问题
        # 根据指标值标记异常点
        # abnormal_points_down = \
        #     df[(df['RSI'] > 70) | (df['收盘'] > df['BB_upper'])]['收盘']
        # plt.scatter(abnormal_points_down.index, abnormal_points_down, c='red', marker='.', label='回调点')
        # abnormal_points_up = \
        #     df[(df['RSI'] < 30) | (df['收盘'] < df['BB_lower'])]['收盘']
        # plt.scatter(abnormal_points_up.index, abnormal_points_up, c='blue', marker='.', label='反弹点')
        # 根据指标值标记异常点
        abnormal_points = df[(df['收盘'] > df['MA20']) & (df['RSI'] > 70)]['收盘']
        plt.scatter(abnormal_points.index, abnormal_points, c='red', marker='^', label='异常点')

        # 图形设置
        plt.title(df['股票名称'][0]+'收盘价随时间变化')
        plt.xlabel('日期')
        plt.ylabel('收盘价')
        plt.legend()
        if len(abnormal_points) >=length:
            result_text.insert(tk.END, f'通过收盘价发现'+df['股票名称'][0]+'收盘价的异常点较多,属于风险股,不建议购买\n\n')
        else:
            result_text.insert(tk.END, df['股票名称'][0]+'收盘价表现正常可以考虑购买\n\n')
        # 显示图像
        plt.show()