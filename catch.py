# 导入 efinance 如果没有安装则需要通过执行命令: pip install efinance 来安装
from typing import Dict
import efinance as ef
import pandas as pd
import time
from datetime import datetime
import tkinter as tk

def get_csv(stock_codes, result_text, start='20220901', end='20221015'):
    # 股票代码或者名称列表
    # stock_codes = ['600519', '腾讯', 'AAPL']
    # 数据间隔时间为 1 分钟
    freq = 1
    status = {stock_code: 0 for stock_code in stock_codes}
    while len(stock_codes) != 0:
        # 获取最新一个交易日的分钟级别股票行情数据
        stocks_df: Dict[str, pd.DataFrame] = ef.stock.get_quote_history(
            stock_codes, start, end=end, klt=freq)
        for stock_code, df in stocks_df.items():
            # 现在的时间
            now = str(datetime.today()).split('.')[0]
            # 将数据存储到 csv 文件中
            df.to_csv(f'data/{stock_code}.csv', encoding='utf-8-sig', index=None)
            result_text.insert(tk.END, f'已在 {now}, 将股票: {stock_code} 的行情数据存储到文件: {stock_code}.csv 中！\n')
            if len(df) == status[stock_code]:
                # 移除已经收盘的股票代码
                stock_codes.remove(stock_code)
                result_text.insert(tk.END, f'股票 {stock_code} 已收盘！\n')
            status[stock_code] = len(df)
        if len(stock_codes) != 0:
            result_text.insert(tk.END, '暂停 30 秒\n')
            time.sleep(30)
        result_text.insert(tk.END, '-' * 10+'\n')

    print('全部股票已收盘')
