import pandas as pd


def wash(stock_codes):
    # 遍历文件列表
    files = [f'data/{code}.csv' for code in stock_codes]
    for file in files:
        # 读取数据文件
        df = pd.read_csv(file, encoding='utf-8')

        # 处理列名和日期格式
        df.columns = ['股票名称', '股票代码', '日期', '开盘', '收盘', '最高', '最低', '成交量', '成交额', '振幅', '涨跌幅',
                      '涨跌额', '换手率']
        df['日期'] = pd.to_datetime(df['日期'])

        # 填充缺失值
        df.fillna(method='ffill', inplace=True)
