import tkinter as tk
from tkinter import filedialog
from catch import get_csv
from 数据清洗 import wash
from 技术指标 import check
from 相关性分析 import corrn
from predict import predict
from 风险控制 import risk_control
from catch import get_csv
from tkinter import ttk
import os
import copy

def menu():

    def on_button_click():
        choose = int(button_var.get())

        if choose == 1:
            check(stock_codes, result_text)
        elif choose == 2:
            corrn(stock_codes, result_text)
        elif choose == 3:
            predict(stock_codes, result_text)
        elif choose == 4:
            risk_control(stock_codes)
        elif choose == 5:
            get_csv(copy.deepcopy(stock_codes), result_text)
        elif choose == 6:
            root.destroy()

    def add_stock():
        stock = entry.get()
        if stock and (stock not in stock_codes):
            stock_codes.append(stock)
            listbox.insert(tk.END, stock)

    def remove_stock():
        selection = listbox.curselection()
        if selection:
            index = selection[0]
            stock = listbox.get(index)
            stock_codes.remove(stock)
            listbox.delete(index)

    def update_file_listbox():
        file_listbox.delete(0, tk.END)
        files = os.listdir("data")
        for file in files:
            file_listbox.insert(tk.END, file)
        root.after(5000, update_file_listbox)  # 5秒后再次调用该函数

    def delete_file():
        # 打开文件选择对话框
        file_path = filedialog.askopenfilename(initialdir="data/", title="Select file to delete",
                                               filetypes=(("CSV files", "*.csv"), ("all files", "*.*")))
        # 弹出确认对话框
        confirm = tk.messagebox.askyesno(title="Confirm Deletion", message="Are you sure to delete the selected file?")
        if confirm:
            os.remove(file_path)
            result_text.insert(tk.END, f"File {file_path} deleted successfully.\n")

    root = tk.Tk()
    root.title("股票数据分析")

    file_listbox_frame = tk.Frame(root)
    file_listbox_frame.grid(row=3, column=2, rowspan=14, sticky=tk.N + tk.S + tk.E + tk.W, padx=5, pady=5)

    file_listbox_scrollbar = tk.Scrollbar(file_listbox_frame)
    file_listbox_scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    file_listbox = tk.Listbox(file_listbox_frame, yscrollcommand=file_listbox_scrollbar.set)
    files = os.listdir("data")
    for file in files:
        file_listbox.insert(tk.END, file)
    file_listbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

    file_listbox_scrollbar.config(command=file_listbox.yview)

    label1 = tk.Label(root, text="请输入股票代码：")
    label1.grid(row=0, column=0, sticky=tk.W)

    entry = tk.Entry(root)
    entry.grid(row=1, column=0, sticky=tk.W)

    add_button = tk.Button(root, text="添加", command=add_stock)
    add_button.grid(row=2, column=0, sticky=tk.W)

    label2 = tk.Label(root, text="请选择操作：")
    label2.grid(row=3, column=0, sticky=tk.W)

    button_var = tk.StringVar()

    button1 = tk.Radiobutton(root, text="技术指标", variable=button_var, value=1)
    button1.grid(row=4, column=0, sticky=tk.W)

    button2 = tk.Radiobutton(root, text="相关性分析", variable=button_var, value=2)
    button2.grid(row=5, column=0, sticky=tk.W)

    button3 = tk.Radiobutton(root, text="收盘值预测", variable=button_var, value=3)
    button3.grid(row=6, column=0, sticky=tk.W)

    button4 = tk.Radiobutton(root, text="风险控制", variable=button_var, value=4)
    button4.grid(row=7, column=0, sticky=tk.W)

    button5 = tk.Radiobutton(root, text="抓取数据", variable=button_var, value=5)
    button5.grid(row=8, column=0, sticky=tk.W)

    button6 = tk.Radiobutton(root, text="退出", variable=button_var, value=6)
    button6.grid(row=9, column=0, sticky=tk.W)

    button = tk.Button(root, text="确定", command=on_button_click)
    button.grid(row=10, column=0, sticky=tk.W)

    delete_button = tk.Button(root, text="删除文件", command=delete_file)  # 添加删除文件按钮
    delete_button.grid(row=2, column=2, sticky=tk.W)

    separator = tk.Frame(height=2, bd=1, relief=tk.SUNKEN)
    separator.grid(row=12, column=0, sticky=tk.W + tk.E, padx=5, pady=5)

    label3 = tk.Label(root, text="已选股票：")
    label3.grid(row=13, column=0, sticky=tk.W)

    listbox_frame = tk.Frame(root)
    listbox_frame.grid(row=14, column=0, sticky=tk.W)

    listbox_scrollbar = tk.Scrollbar(listbox_frame)
    listbox_scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    listbox = tk.Listbox(listbox_frame, yscrollcommand=listbox_scrollbar.set)
    for stock in stock_codes:
        listbox.insert(tk.END, stock)
    listbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

    listbox_scrollbar.config(command=listbox.yview)

    remove_button = tk.Button(root, text="删除", command=remove_stock)
    remove_button.grid(row=15, column=0, sticky=tk.W)

    result_frame = tk.Frame(root)
    result_frame.grid(row=0, column=1, rowspan=20, sticky=tk.N + tk.S + tk.E + tk.W, padx=5, pady=5)

    result_scrollbar = tk.Scrollbar(result_frame)
    result_scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    result_text = tk.Text(result_frame, yscrollcommand=result_scrollbar.set)
    result_text.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

    result_scrollbar.config(command=result_text.yview)
    label4 = tk.Label(root, text="数据文件列表：")
    label4.grid(row=1, column=2, sticky=tk.W)
    update_file_listbox()
    root.mainloop()


if __name__ == "__main__":
    stock_codes = ['00700', 'AMZN', 'BABA']
    menu()
